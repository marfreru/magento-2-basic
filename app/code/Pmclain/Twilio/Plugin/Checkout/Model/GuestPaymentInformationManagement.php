<?php
/**
 * Pmclain_Twilio extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GPL v3 License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://www.gnu.org/licenses/gpl.txt
 *
 * @category       Pmclain
 * @package        Twilio
 * @copyright      Copyright (c) 2017
 * @license        https://www.gnu.org/licenses/gpl.txt GPL v3 License
 */

namespace Pmclain\Twilio\Plugin\Checkout\Model;

use Psr\Log\LoggerInterface;

class GuestPaymentInformationManagement
{
    protected $_logger;

    public function __construct(
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_logger = $logger;
    }


    public function beforeSavePaymentInformation(
        \Magento\Checkout\Model\GuestPaymentInformationManagement $subject,
        $cartId,
        $email,
        \Magento\Quote\Api\Data\PaymentInterface $paymentMethod,
        \Magento\Quote\Api\Data\AddressInterface $billingAddress = null
    ) {
        $level = 'INFO';
        $this->_logger->log($level, 'app/code/Pmclain/Twilio/Plugin/Checkout/Model/GuestPaymentInformationManagement', [ '$billingAddress' => $billingAddress ]);

        if (!$billingAddress) {
            return;
        }

        // if ($billingAddress->getExtensionAttributes()) {
        //     $billingAddress->setSmsAlert((int)$billingAddress->getExtensionAttributes()->getSmsAlert());
        // } else {
        //     $billingAddress->setSmsAlert(0);
        // }
    }
}
