<?php
/**
 * Pmclain_Twilio extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GPL v3 License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://www.gnu.org/licenses/gpl.txt
 *
 * @category       Pmclain
 * @package        Twilio
 * @copyright      Copyright (c) 2017
 * @license        https://www.gnu.org/licenses/gpl.txt GPL v3 License
 */

namespace Pmclain\Twilio\Plugin\Checkout\Model;

class ShippingInformationManagement
{
    protected $_logger;

    public function __construct(
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_logger = $logger;
    }

    public function beforeSaveAddressInformation(
        \Magento\Checkout\Model\ShippingInformationManagement $subject,
        $cartId,
        \Magento\Checkout\Api\Data\ShippingInformationInterface $addressInformation
    ) {
        $level = 'INFO';
        // $this->_logger->log($level, 'app/code/Pmclain/Twilio/Plugin/Checkout/Model/ShippingInformationManagement', [ '$addressInformation' => $addressInformation ]);

        $shippingAddress = $addressInformation->getShippingAddress();
        $billingAddress = $addressInformation->getBillingAddress();

        // $this->_logger->log($level, 'app/code/Pmclain/Twilio/Plugin/Checkout/Model/ShippingInformationManagement', [ '$shippingAddress' => json_encode( $shippingAddress->getExtensionAttributes() ) ]);
        // $this->_logger->log($level, 'app/code/Pmclain/Twilio/Plugin/Checkout/Model/ShippingInformationManagement', [ '$shippingAddress' => json_encode( $shippingAddress ) ]);
        // $this->_logger->log($level, 'app/code/Pmclain/Twilio/Plugin/Checkout/Model/ShippingInformationManagement', [ '$shippingAddress' =>  get_class_methods( $shippingAddress ) ]);
        // $this->_logger->log($level, 'app/code/Pmclain/Twilio/Plugin/Checkout/Model/ShippingInformationManagement', [ '$shippingAddress' =>  $shippingAddress->getLastName() ]);
        // $this->_logger->log($level, 'app/code/Pmclain/Twilio/Plugin/Checkout/Model/ShippingInformationManagement', [ '$shippingAddress' =>  get_class_methods( $shippingAddress->getExtensionAttributes() ) ]);
        // $this->_logger->log($level, 'app/code/Pmclain/Twilio/Plugin/Checkout/Model/ShippingInformationManagement', [ '$shippingAddress' =>  $shippingAddress->getExtensionAttributes()->getLatLong() ]);
        $this->_logger->log($level, 'app/code/Pmclain/Twilio/Plugin/Checkout/Model/ShippingInformationManagement', [ '$billingAddress' =>  $billingAddress->getExtensionAttributes()->getLatLong() ]);

        if ($shippingAddress->getExtensionAttributes()) {
            // $shippingAddress->setSmsAlert((int)$shippingAddress->getExtensionAttributes()->getSmsAlert());
            $shippingAddress->setLatLong( $shippingAddress->getExtensionAttributes()->getLatLong());
        }

        // if ($billingAddress->getExtensionAttributes()) {
        //     $billingAddress->setSmsAlert((int)$billingAddress->getExtensionAttributes()->getSmsAlert());
        // } else {
        //     $billingAddress->setSmsAlert(0);
        // }
    }
}
