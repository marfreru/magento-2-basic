/**
 * Pmclain_Twilio extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the GPL v3 License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://www.gnu.org/licenses/gpl.txt
 *
 * @category       Pmclain
 * @package        Twilio
 * @copyright      Copyright (c) 2017
 * @license        https://www.gnu.org/licenses/gpl.txt GPL v3 License
 */

define([
  'mage/utils/wrapper'
], function(wrapper) {
  'use strict';

  return function (createBillingAddressAction) {
    return wrapper.wrap(createBillingAddressAction, function(originalAction, addressData) {

      console.log("create-billing-address mixin addressData", addressData);
      
      if (addressData.custom_attributes === undefined) {
        return originalAction();
      }

      // if (addressData.custom_attributes['lat_long']) {
      //   addressData.custom_attributes['lat_long'] = {
      //     'attribute_code': 'lat_long',
      //     'value': 'SMS Enabled',
      //     'status': 1
      //   }
      // }

      return originalAction();
    });
  };
});